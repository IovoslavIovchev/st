# the simple (suckless) terminal

The [suckless terminal (st)](https://st.suckless.org/) with some additional features, mainly gruvbox prettiness.

Based on [Luke Smith's fork](https://github.com/lukesmithxyz/st).

## Installation

```
sudo make clean install
```
